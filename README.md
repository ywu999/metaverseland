# Metaverse Land Development

## Environment

The developer should have installed git, python3, MetaMask. It also should have some balance in ropsten test net in order to run this project. To get started,

```
git clone https://gitlab.com/ywu999/metaverseland.git
python -m http.server
http://localhost:8000/MoraLand_Frontend/
```
